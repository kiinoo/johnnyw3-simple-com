// MFCActiveXControl2PropPage.cpp : CMFCActiveXControl2PropPage 属性页类的实现。

#include "stdafx.h"
#include "MFCActiveXControl2.h"
#include "MFCActiveXControl2PropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMFCActiveXControl2PropPage, COlePropertyPage)

// 消息映射

BEGIN_MESSAGE_MAP(CMFCActiveXControl2PropPage, COlePropertyPage)
END_MESSAGE_MAP()

// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CMFCActiveXControl2PropPage, "MFCACTIVEXCONT.MFCActiveXContPropPage.1",
	0x258d16ec, 0x135f, 0x4a0a, 0x83, 0xc9, 0xc7, 0x47, 0x1b, 0x41, 0x27, 0x30)

// CMFCActiveXControl2PropPage::CMFCActiveXControl2PropPageFactory::UpdateRegistry -
// 添加或移除 CMFCActiveXControl2PropPage 的系统注册表项

BOOL CMFCActiveXControl2PropPage::CMFCActiveXControl2PropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_MFCACTIVEXCONTROL2_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}

// CMFCActiveXControl2PropPage::CMFCActiveXControl2PropPage - 构造函数

CMFCActiveXControl2PropPage::CMFCActiveXControl2PropPage() :
	COlePropertyPage(IDD, IDS_MFCACTIVEXCONTROL2_PPG_CAPTION)
{
}

// CMFCActiveXControl2PropPage::DoDataExchange - 在页和属性间移动数据

void CMFCActiveXControl2PropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}

// CMFCActiveXControl2PropPage 消息处理程序
