#pragma once

// MFCActiveXControl2Ctrl.h : CMFCActiveXControl2Ctrl ActiveX 控件类的声明。


// CMFCActiveXControl2Ctrl : 有关实现的信息，请参阅 MFCActiveXControl2Ctrl.cpp。

class CMFCActiveXControl2Ctrl : public COleControl
{
	DECLARE_DYNCREATE(CMFCActiveXControl2Ctrl)

// 构造函数
public:
	CMFCActiveXControl2Ctrl();

// 重写
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// 实现
protected:
	~CMFCActiveXControl2Ctrl();

	DECLARE_OLECREATE_EX(CMFCActiveXControl2Ctrl)    // 类工厂和 guid
	DECLARE_OLETYPELIB(CMFCActiveXControl2Ctrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CMFCActiveXControl2Ctrl)     // 属性页 ID
	DECLARE_OLECTLTYPE(CMFCActiveXControl2Ctrl)		// 类型名称和杂项状态

// 消息映射
	DECLARE_MESSAGE_MAP()

// 调度映射
	DECLARE_DISPATCH_MAP()

// 事件映射
	DECLARE_EVENT_MAP()

// 调度和事件 ID
public:
	enum {
		dispidGetANumber = 1L
	};
protected:
	LONG GetANumber();
};

