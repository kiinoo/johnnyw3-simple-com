// MFCActiveXControl2.cpp : CMFCActiveXControl2App 和 DLL 注册的实现。

#include "stdafx.h"
#include "MFCActiveXControl2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CMFCActiveXControl2App theApp;

const GUID CDECL _tlid = { 0x4103FD17, 0x2BAD, 0x4AE0, { 0x9C, 0x7E, 0xE5, 0xBB, 0x6, 0xC5, 0x83, 0x7F } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CMFCActiveXControl2App::InitInstance - DLL 初始化

BOOL CMFCActiveXControl2App::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO:  在此添加您自己的模块初始化代码。
	}

	return bInit;
}



// CMFCActiveXControl2App::ExitInstance - DLL 终止

int CMFCActiveXControl2App::ExitInstance()
{
	// TODO:  在此添加您自己的模块终止代码。

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - 将项添加到系统注册表

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - 将项从系统注册表中移除

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
