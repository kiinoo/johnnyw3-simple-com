#pragma once

// MFCActiveXControl2PropPage.h : CMFCActiveXControl2PropPage 属性页类的声明。


// CMFCActiveXControl2PropPage : 有关实现的信息，请参阅 MFCActiveXControl2PropPage.cpp。

class CMFCActiveXControl2PropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CMFCActiveXControl2PropPage)
	DECLARE_OLECREATE_EX(CMFCActiveXControl2PropPage)

// 构造函数
public:
	CMFCActiveXControl2PropPage();

// 对话框数据
	enum { IDD = IDD_PROPPAGE_MFCACTIVEXCONTROL2 };

// 实现
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 消息映射
protected:
	DECLARE_MESSAGE_MAP()
};

