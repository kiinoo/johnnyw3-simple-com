// MFCActiveXControl2Ctrl.cpp : CMFCActiveXControl2Ctrl ActiveX 控件类的实现。

#include "stdafx.h"
#include "MFCActiveXControl2.h"
#include "MFCActiveXControl2Ctrl.h"
#include "MFCActiveXControl2PropPage.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMFCActiveXControl2Ctrl, COleControl)

// 消息映射

BEGIN_MESSAGE_MAP(CMFCActiveXControl2Ctrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()

// 调度映射

BEGIN_DISPATCH_MAP(CMFCActiveXControl2Ctrl, COleControl)
	DISP_FUNCTION_ID(CMFCActiveXControl2Ctrl, "GetANumber", dispidGetANumber, GetANumber, VT_I4, VTS_NONE)
END_DISPATCH_MAP()

// 事件映射

BEGIN_EVENT_MAP(CMFCActiveXControl2Ctrl, COleControl)
END_EVENT_MAP()

// 属性页

// TODO:  按需要添加更多属性页。  请记住增加计数!
BEGIN_PROPPAGEIDS(CMFCActiveXControl2Ctrl, 1)
	PROPPAGEID(CMFCActiveXControl2PropPage::guid)
END_PROPPAGEIDS(CMFCActiveXControl2Ctrl)

// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CMFCActiveXControl2Ctrl, "MFCACTIVEXCONTRO.MFCActiveXControCtrl.1",
	0x372894e4, 0x39e7, 0x4944, 0xb4, 0xea, 0xc8, 0x6d, 0x6c, 0x40, 0x8a, 0xaf)

// 键入库 ID 和版本

IMPLEMENT_OLETYPELIB(CMFCActiveXControl2Ctrl, _tlid, _wVerMajor, _wVerMinor)

// 接口 ID

const IID IID_DMFCActiveXControl2 = { 0x659BCE4D, 0x17B8, 0x4646, { 0xAD, 0x4, 0x60, 0xCE, 0xC, 0xB8, 0xF4, 0xB6 } };
const IID IID_DMFCActiveXControl2Events = { 0xB57C0C06, 0x8F7E, 0x4039, { 0x88, 0xAE, 0x30, 0x2D, 0x8D, 0xB2, 0x2A, 0x23 } };

// 控件类型信息

static const DWORD _dwMFCActiveXControl2OleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CMFCActiveXControl2Ctrl, IDS_MFCACTIVEXCONTROL2, _dwMFCActiveXControl2OleMisc)

// CMFCActiveXControl2Ctrl::CMFCActiveXControl2CtrlFactory::UpdateRegistry -
// 添加或移除 CMFCActiveXControl2Ctrl 的系统注册表项

BOOL CMFCActiveXControl2Ctrl::CMFCActiveXControl2CtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO:  验证您的控件是否符合单元模型线程处理规则。
	// 有关更多信息，请参考 MFC 技术说明 64。
	// 如果您的控件不符合单元模型规则，则
	// 必须修改如下代码，将第六个参数从
	// afxRegApartmentThreading 改为 0。

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_MFCACTIVEXCONTROL2,
			IDB_MFCACTIVEXCONTROL2,
			afxRegApartmentThreading,
			_dwMFCActiveXControl2OleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


// CMFCActiveXControl2Ctrl::CMFCActiveXControl2Ctrl - 构造函数

CMFCActiveXControl2Ctrl::CMFCActiveXControl2Ctrl()
{
	InitializeIIDs(&IID_DMFCActiveXControl2, &IID_DMFCActiveXControl2Events);
	// TODO:  在此初始化控件的实例数据。
}

// CMFCActiveXControl2Ctrl::~CMFCActiveXControl2Ctrl - 析构函数

CMFCActiveXControl2Ctrl::~CMFCActiveXControl2Ctrl()
{
	// TODO:  在此清理控件的实例数据。
}

// CMFCActiveXControl2Ctrl::OnDraw - 绘图函数

void CMFCActiveXControl2Ctrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& /* rcInvalid */)
{
	if (!pdc)
		return;

	// TODO:  用您自己的绘图代码替换下面的代码。
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}

// CMFCActiveXControl2Ctrl::DoPropExchange - 持久性支持

void CMFCActiveXControl2Ctrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO:  为每个持久的自定义属性调用 PX_ 函数。
}


// CMFCActiveXControl2Ctrl::OnResetState - 将控件重置为默认状态

void CMFCActiveXControl2Ctrl::OnResetState()
{
	COleControl::OnResetState();  // 重置 DoPropExchange 中找到的默认值

	// TODO:  在此重置任意其他控件状态。
}


// CMFCActiveXControl2Ctrl 消息处理程序


LONG CMFCActiveXControl2Ctrl::GetANumber()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO:  在此添加调度处理程序代码

	return 0;
}
