// MFCAc.cpp : CMFCAcApp 和 DLL 注册的实现。

#include "stdafx.h"
#include "MFCAc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CMFCAcApp theApp;

const GUID CDECL _tlid = { 0x1037D34B, 0x3D23, 0x4A03, { 0xB4, 0x27, 0xC5, 0xC1, 0x55, 0x6D, 0x2, 0x19 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CMFCAcApp::InitInstance - DLL 初始化

BOOL CMFCAcApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO:  在此添加您自己的模块初始化代码。
	}

	return bInit;
}



// CMFCAcApp::ExitInstance - DLL 终止

int CMFCAcApp::ExitInstance()
{
	// TODO:  在此添加您自己的模块终止代码。

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - 将项添加到系统注册表

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - 将项从系统注册表中移除

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
