// MFCAcPropPage.cpp : CMFCAcPropPage 属性页类的实现。

#include "stdafx.h"
#include "MFCAc.h"
#include "MFCAcPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMFCAcPropPage, COlePropertyPage)

// 消息映射

BEGIN_MESSAGE_MAP(CMFCAcPropPage, COlePropertyPage)
END_MESSAGE_MAP()

// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CMFCAcPropPage, "MFCAC.MFCAcPropPage.1",
	0x64d5c2a7, 0x42b3, 0x4d39, 0x9c, 0x89, 0x21, 0x58, 0xd1, 0x36, 0xe4, 0xfc)

// CMFCAcPropPage::CMFCAcPropPageFactory::UpdateRegistry -
// 添加或移除 CMFCAcPropPage 的系统注册表项

BOOL CMFCAcPropPage::CMFCAcPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_MFCAC_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}

// CMFCAcPropPage::CMFCAcPropPage - 构造函数

CMFCAcPropPage::CMFCAcPropPage() :
	COlePropertyPage(IDD, IDS_MFCAC_PPG_CAPTION)
{
}

// CMFCAcPropPage::DoDataExchange - 在页和属性间移动数据

void CMFCAcPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}

// CMFCAcPropPage 消息处理程序
