// MFCAcCtrl.cpp : CMFCAcCtrl ActiveX 控件类的实现。

#include "stdafx.h"
#include "MFCAc.h"
#include "MFCAcCtrl.h"
#include "MFCAcPropPage.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMFCAcCtrl, COleControl)

// 消息映射

BEGIN_MESSAGE_MAP(CMFCAcCtrl, COleControl)
	ON_MESSAGE(OCM_COMMAND, &CMFCAcCtrl::OnOcmCommand)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()

// 调度映射

BEGIN_DISPATCH_MAP(CMFCAcCtrl, COleControl)
	DISP_FUNCTION_ID(CMFCAcCtrl, "GetNum", dispidGetNum, GetNum, VT_I4, VTS_NONE)
	DISP_FUNCTION_ID(CMFCAcCtrl, "GetMath", dispidGetMath, GetMath, VT_I4, VTS_NONE)
END_DISPATCH_MAP()

// 事件映射

BEGIN_EVENT_MAP(CMFCAcCtrl, COleControl)
END_EVENT_MAP()

// 属性页

// TODO:  按需要添加更多属性页。  请记住增加计数!
BEGIN_PROPPAGEIDS(CMFCAcCtrl, 1)
	PROPPAGEID(CMFCAcPropPage::guid)
END_PROPPAGEIDS(CMFCAcCtrl)

// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CMFCAcCtrl, "MFCAC.MFCAcCtrl.1",
	0x739f31df, 0xfa1f, 0x4b46, 0xa1, 0xa4, 0x99, 0xe, 0x6c, 0x5, 0xc4, 0x66)

// 键入库 ID 和版本

IMPLEMENT_OLETYPELIB(CMFCAcCtrl, _tlid, _wVerMajor, _wVerMinor)

// 接口 ID

const IID IID_DMFCAc = { 0x408998AD, 0x5314, 0x4164, { 0x95, 0xB, 0xB9, 0x13, 0x8, 0x5E, 0x23, 0x37 } };
const IID IID_DMFCAcEvents = { 0xD0703BB7, 0x1589, 0x43C6, { 0x8A, 0xDF, 0x77, 0x7D, 0x4D, 0xA8, 0x10, 0xE1 } };

// 控件类型信息

static const DWORD _dwMFCAcOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CMFCAcCtrl, IDS_MFCAC, _dwMFCAcOleMisc)

// CMFCAcCtrl::CMFCAcCtrlFactory::UpdateRegistry -
// 添加或移除 CMFCAcCtrl 的系统注册表项

BOOL CMFCAcCtrl::CMFCAcCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO:  验证您的控件是否符合单元模型线程处理规则。
	// 有关更多信息，请参考 MFC 技术说明 64。
	// 如果您的控件不符合单元模型规则，则
	// 必须修改如下代码，将第六个参数从
	// afxRegApartmentThreading 改为 0。

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_MFCAC,
			IDB_MFCAC,
			afxRegApartmentThreading,
			_dwMFCAcOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


// CMFCAcCtrl::CMFCAcCtrl - 构造函数

CMFCAcCtrl::CMFCAcCtrl()
{
	InitializeIIDs(&IID_DMFCAc, &IID_DMFCAcEvents);
	// TODO:  在此初始化控件的实例数据。
}

// CMFCAcCtrl::~CMFCAcCtrl - 析构函数

CMFCAcCtrl::~CMFCAcCtrl()
{
	// TODO:  在此清理控件的实例数据。
}

// CMFCAcCtrl::OnDraw - 绘图函数

void CMFCAcCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& /* rcInvalid */)
{
	if (!pdc)
		return;

	DoSuperclassPaint(pdc, rcBounds);
}

// CMFCAcCtrl::DoPropExchange - 持久性支持

void CMFCAcCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO:  为每个持久的自定义属性调用 PX_ 函数。
}


// CMFCAcCtrl::OnResetState - 将控件重置为默认状态

void CMFCAcCtrl::OnResetState()
{
	COleControl::OnResetState();  // 重置 DoPropExchange 中找到的默认值

	// TODO:  在此重置任意其他控件状态。
}


// CMFCAcCtrl::PreCreateWindow - 修改 CreateWindowEx 的参数

BOOL CMFCAcCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.lpszClass = _T("STATIC");
	return COleControl::PreCreateWindow(cs);
}

// CMFCAcCtrl::IsSubclassedControl - 这是一个子类控件

BOOL CMFCAcCtrl::IsSubclassedControl()
{
	return TRUE;
}

// CMFCAcCtrl::OnOcmCommand - 处理命令消息

LRESULT CMFCAcCtrl::OnOcmCommand(WPARAM wParam, LPARAM lParam)
{
	WORD wNotifyCode = HIWORD(wParam);

	// TODO:  在此接通 wNotifyCode。

	return 0;
}


// CMFCAcCtrl 消息处理程序


LONG CMFCAcCtrl::GetNum()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO:  在此添加调度处理程序代码

	return 0;
}


LONG CMFCAcCtrl::GetMath()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO:  在此添加调度处理程序代码

	return 0;
}
