#pragma once

// MFCAcCtrl.h : CMFCAcCtrl ActiveX 控件类的声明。


// CMFCAcCtrl : 有关实现的信息，请参阅 MFCAcCtrl.cpp。

class CMFCAcCtrl : public COleControl
{
	DECLARE_DYNCREATE(CMFCAcCtrl)

// 构造函数
public:
	CMFCAcCtrl();

// 重写
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// 实现
protected:
	~CMFCAcCtrl();

	DECLARE_OLECREATE_EX(CMFCAcCtrl)    // 类工厂和 guid
	DECLARE_OLETYPELIB(CMFCAcCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CMFCAcCtrl)     // 属性页 ID
	DECLARE_OLECTLTYPE(CMFCAcCtrl)		// 类型名称和杂项状态

	// 子类控件支持
	BOOL IsSubclassedControl();
	LRESULT OnOcmCommand(WPARAM wParam, LPARAM lParam);

// 消息映射
	DECLARE_MESSAGE_MAP()

// 调度映射
	DECLARE_DISPATCH_MAP()

// 事件映射
	DECLARE_EVENT_MAP()

// 调度和事件 ID
public:
	enum {
		dispidGetMath = 1L,
		dispidGetNum = 1L
	};
protected:
	LONG GetNum();
	LONG GetMath();
};

