#pragma once

// MFCAcPropPage.h : CMFCAcPropPage 属性页类的声明。


// CMFCAcPropPage : 有关实现的信息，请参阅 MFCAcPropPage.cpp。

class CMFCAcPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CMFCAcPropPage)
	DECLARE_OLECREATE_EX(CMFCAcPropPage)

// 构造函数
public:
	CMFCAcPropPage();

// 对话框数据
	enum { IDD = IDD_PROPPAGE_MFCAC };

// 实现
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 消息映射
protected:
	DECLARE_MESSAGE_MAP()
};

