

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Mon Jun 13 16:19:27 2011
 */
/* Compiler settings for .\SFTComServer.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __SFTComServer_h__
#define __SFTComServer_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IMathControl_FWD_DEFINED__
#define __IMathControl_FWD_DEFINED__
typedef interface IMathControl IMathControl;
#endif 	/* __IMathControl_FWD_DEFINED__ */


#ifndef __MathControl_FWD_DEFINED__
#define __MathControl_FWD_DEFINED__

#ifdef __cplusplus
typedef class MathControl MathControl;
#else
typedef struct MathControl MathControl;
#endif /* __cplusplus */

#endif 	/* __MathControl_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IMathControl_INTERFACE_DEFINED__
#define __IMathControl_INTERFACE_DEFINED__

/* interface IMathControl */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMathControl;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B129C4F7-81EA-47DA-AA22-4120C487C3FB")
    IMathControl : public IDispatch
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE AddTwoNumbers( 
            /* [in] */ DWORD Number1,
            /* [in] */ DWORD Number2,
            /* [out] */ DWORD *pResult) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMathControlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMathControl * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMathControl * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMathControl * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMathControl * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMathControl * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMathControl * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMathControl * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        HRESULT ( STDMETHODCALLTYPE *AddTwoNumbers )( 
            IMathControl * This,
            /* [in] */ DWORD Number1,
            /* [in] */ DWORD Number2,
            /* [out] */ DWORD *pResult);
        
        END_INTERFACE
    } IMathControlVtbl;

    interface IMathControl
    {
        CONST_VTBL struct IMathControlVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMathControl_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMathControl_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMathControl_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMathControl_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IMathControl_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IMathControl_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IMathControl_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IMathControl_AddTwoNumbers(This,Number1,Number2,pResult)	\
    (This)->lpVtbl -> AddTwoNumbers(This,Number1,Number2,pResult)

#endif /* COBJMACROS */


#endif 	/* C style interface */



HRESULT STDMETHODCALLTYPE IMathControl_AddTwoNumbers_Proxy( 
    IMathControl * This,
    /* [in] */ DWORD Number1,
    /* [in] */ DWORD Number2,
    /* [out] */ DWORD *pResult);


void __RPC_STUB IMathControl_AddTwoNumbers_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMathControl_INTERFACE_DEFINED__ */



#ifndef __SFTComServerLib_LIBRARY_DEFINED__
#define __SFTComServerLib_LIBRARY_DEFINED__

/* library SFTComServerLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_SFTComServerLib;

EXTERN_C const CLSID CLSID_MathControl;

#ifdef __cplusplus

class DECLSPEC_UUID("005C1FA1-FC7E-4FBB-8F12-15D410416F59")
MathControl;
#endif
#endif /* __SFTComServerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


