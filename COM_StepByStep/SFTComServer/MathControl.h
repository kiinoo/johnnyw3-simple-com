// MathControl.h : Declaration of the CMathControl

#pragma once
#include "resource.h"       // main symbols

#include "SFTComServer.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CMathControl

class ATL_NO_VTABLE CMathControl :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CMathControl, &CLSID_MathControl>,
	public IDispatchImpl<IMathControl, &IID_IMathControl, &LIBID_SFTComServerLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CMathControl()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_MATHCONTROL)


BEGIN_COM_MAP(CMathControl)
	COM_INTERFACE_ENTRY(IMathControl)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

    virtual HRESULT STDMETHODCALLTYPE AddTwoNumbers( 
            /* [in] */ DWORD Number1,
            /* [in] */ DWORD Number2,
            /* [out] */ DWORD *pResult) 
    {
        *pResult = Number1 + Number2;
        return S_OK ;
    }

};

OBJECT_ENTRY_AUTO(__uuidof(MathControl), CMathControl)
