// SFTComClient.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <tchar.h>

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <atlbase.h>
#include <atlstr.h>

#include <iostream>

#include "../SFTComServer/SFTComServer.h"
#include "../SFTComServer/SFTComServer_i.c"


int _tmain(int argc, _TCHAR* argv[])
{
    ::CoInitialize(NULL);
    IMathControl *pmc = NULL;

    HRESULT hResult = ::CoCreateInstance(CLSID_MathControl, NULL, CLSCTX_INPROC_SERVER, IID_IMathControl, (LPVOID*) &pmc);

    if(FAILED(hResult))
    {
        std::cout << "Failed in creating COM Component";
        return 0;
    }

    DWORD result = 0;
    pmc->AddTwoNumbers(10, 15, &result);
    std::cout << "Result : " << result << "\n";
    pmc->AddTwoNumbers(10, 25, &result);
    std::cout << "Result : " << result << "\n";
    pmc->AddTwoNumbers(10, 35, &result);
    std::cout << "Result : " << result << "\n\n";

	return 0;
}

