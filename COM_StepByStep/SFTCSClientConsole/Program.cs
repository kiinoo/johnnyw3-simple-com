using System;
using System.Collections.Generic;
using System.Text;

namespace SFTCSClientConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SFTComServer.MathControl ctrl = new SFTComServer.MathControl();
                uint result = 0;
                ctrl.AddTwoNumbers(10, 15, out result);
                Console.WriteLine(result.ToString());
                ctrl.AddTwoNumbers(10, 25, out result);
                Console.WriteLine(result.ToString());
                ctrl.AddTwoNumbers(10, 35, out result);
                Console.WriteLine(result.ToString());
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }
    }
}
