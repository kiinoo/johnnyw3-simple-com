// SimpleActiveXControlCtrl.cpp : CSimpleActiveXControlCtrl ActiveX 控件类的实现。

#include "stdafx.h"
#include "SimpleActiveXControl.h"
#include "SimpleActiveXControlCtrl.h"
#include "SimpleActiveXControlPropPage.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CSimpleActiveXControlCtrl, COleControl)

// 消息映射

BEGIN_MESSAGE_MAP(CSimpleActiveXControlCtrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()

// 调度映射

BEGIN_DISPATCH_MAP(CSimpleActiveXControlCtrl, COleControl)
	DISP_FUNCTION_ID(CSimpleActiveXControlCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION_ID(CSimpleActiveXControlCtrl, "GetSomeNumber", dispidGetSomeNumber, GetSomeNumber, VT_I4, VTS_NONE)
END_DISPATCH_MAP()

// 事件映射

BEGIN_EVENT_MAP(CSimpleActiveXControlCtrl, COleControl)
END_EVENT_MAP()

// 属性页

// TODO:  按需要添加更多属性页。  请记住增加计数!
BEGIN_PROPPAGEIDS(CSimpleActiveXControlCtrl, 1)
	PROPPAGEID(CSimpleActiveXControlPropPage::guid)
END_PROPPAGEIDS(CSimpleActiveXControlCtrl)

// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CSimpleActiveXControlCtrl, "SIMPLEACTIVEXCON.SimpleActiveXConCtrl.1",
	0x2092d2e2, 0x82e8, 0x43d6, 0xaa, 0x7b, 0xbf, 0xd5, 0xad, 0xd6, 0x8b, 0x91)

// 键入库 ID 和版本

IMPLEMENT_OLETYPELIB(CSimpleActiveXControlCtrl, _tlid, _wVerMajor, _wVerMinor)

// 接口 ID

const IID IID_DSimpleActiveXControl = { 0xA39F7BFA, 0x4D9D, 0x4990, { 0x92, 0x15, 0xCB, 0x82, 0x2D, 0xDD, 0x17, 0x35 } };
const IID IID_DSimpleActiveXControlEvents = { 0xD4D14A07, 0x6094, 0x4838, { 0x98, 0x67, 0xCB, 0x71, 0xAE, 0x8B, 0x72, 0x22 } };

// 控件类型信息

static const DWORD _dwSimpleActiveXControlOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CSimpleActiveXControlCtrl, IDS_SIMPLEACTIVEXCONTROL, _dwSimpleActiveXControlOleMisc)

// CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrlFactory::UpdateRegistry -
// 添加或移除 CSimpleActiveXControlCtrl 的系统注册表项

BOOL CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO:  验证您的控件是否符合单元模型线程处理规则。
	// 有关更多信息，请参考 MFC 技术说明 64。
	// 如果您的控件不符合单元模型规则，则
	// 必须修改如下代码，将第六个参数从
	// afxRegApartmentThreading 改为 0。

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_SIMPLEACTIVEXCONTROL,
			IDB_SIMPLEACTIVEXCONTROL,
			afxRegApartmentThreading,
			_dwSimpleActiveXControlOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


// CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrl - 构造函数

CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrl()
{
	InitializeIIDs(&IID_DSimpleActiveXControl, &IID_DSimpleActiveXControlEvents);
	// TODO:  在此初始化控件的实例数据。
}

// CSimpleActiveXControlCtrl::~CSimpleActiveXControlCtrl - 析构函数

CSimpleActiveXControlCtrl::~CSimpleActiveXControlCtrl()
{
	// TODO:  在此清理控件的实例数据。
}

// CSimpleActiveXControlCtrl::OnDraw - 绘图函数

void CSimpleActiveXControlCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& /* rcInvalid */)
{
	if (!pdc)
		return;

	// TODO:  用您自己的绘图代码替换下面的代码。
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}

// CSimpleActiveXControlCtrl::DoPropExchange - 持久性支持

void CSimpleActiveXControlCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO:  为每个持久的自定义属性调用 PX_ 函数。
}


// CSimpleActiveXControlCtrl::OnResetState - 将控件重置为默认状态

void CSimpleActiveXControlCtrl::OnResetState()
{
	COleControl::OnResetState();  // 重置 DoPropExchange 中找到的默认值

	// TODO:  在此重置任意其他控件状态。
}


// CSimpleActiveXControlCtrl::AboutBox - 向用户显示“关于”框

void CSimpleActiveXControlCtrl::AboutBox()
{
	CDialogEx dlgAbout(IDD_ABOUTBOX_SIMPLEACTIVEXCONTROL);
	dlgAbout.DoModal();
}


// CSimpleActiveXControlCtrl 消息处理程序


LONG CSimpleActiveXControlCtrl::GetSomeNumber()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO:  在此添加调度处理程序代码

	return 9999;
}
