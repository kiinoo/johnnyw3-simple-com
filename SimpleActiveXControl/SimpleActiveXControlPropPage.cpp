// SimpleActiveXControlPropPage.cpp : CSimpleActiveXControlPropPage 属性页类的实现。

#include "stdafx.h"
#include "SimpleActiveXControl.h"
#include "SimpleActiveXControlPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CSimpleActiveXControlPropPage, COlePropertyPage)

// 消息映射

BEGIN_MESSAGE_MAP(CSimpleActiveXControlPropPage, COlePropertyPage)
END_MESSAGE_MAP()

// 初始化类工厂和 guid

IMPLEMENT_OLECREATE_EX(CSimpleActiveXControlPropPage, "SIMPLEACTIVEXC.SimpleActiveXCPropPage.1",
	0x8f636615, 0xa3af, 0x4dab, 0xa8, 0xc0, 0x4e, 0xfa, 0x38, 0x23, 0xa5, 0)

// CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPageFactory::UpdateRegistry -
// 添加或移除 CSimpleActiveXControlPropPage 的系统注册表项

BOOL CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_SIMPLEACTIVEXCONTROL_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}

// CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPage - 构造函数

CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPage() :
	COlePropertyPage(IDD, IDS_SIMPLEACTIVEXCONTROL_PPG_CAPTION)
{
}

// CSimpleActiveXControlPropPage::DoDataExchange - 在页和属性间移动数据

void CSimpleActiveXControlPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}

// CSimpleActiveXControlPropPage 消息处理程序
