// SimpleActiveXControl.cpp : CSimpleActiveXControlApp 和 DLL 注册的实现。

#include "stdafx.h"
#include "SimpleActiveXControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CSimpleActiveXControlApp theApp;

const GUID CDECL _tlid = { 0xADAFD1EF, 0x5908, 0x4F93, { 0x95, 0x96, 0x5E, 0x9C, 0x2E, 0x68, 0x7E, 0x92 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 3;



// CSimpleActiveXControlApp::InitInstance - DLL 初始化

BOOL CSimpleActiveXControlApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO:  在此添加您自己的模块初始化代码。
	}

	return bInit;
}



// CSimpleActiveXControlApp::ExitInstance - DLL 终止

int CSimpleActiveXControlApp::ExitInstance()
{
	// TODO:  在此添加您自己的模块终止代码。

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - 将项添加到系统注册表

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - 将项从系统注册表中移除

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
