#pragma once

// SimpleActiveXControlCtrl.h : CSimpleActiveXControlCtrl ActiveX 控件类的声明。


// CSimpleActiveXControlCtrl : 有关实现的信息，请参阅 SimpleActiveXControlCtrl.cpp。

class CSimpleActiveXControlCtrl : public COleControl
{
	DECLARE_DYNCREATE(CSimpleActiveXControlCtrl)

// 构造函数
public:
	CSimpleActiveXControlCtrl();

// 重写
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// 实现
protected:
	~CSimpleActiveXControlCtrl();

	DECLARE_OLECREATE_EX(CSimpleActiveXControlCtrl)    // 类工厂和 guid
	DECLARE_OLETYPELIB(CSimpleActiveXControlCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CSimpleActiveXControlCtrl)     // 属性页 ID
	DECLARE_OLECTLTYPE(CSimpleActiveXControlCtrl)		// 类型名称和杂项状态

// 消息映射
	DECLARE_MESSAGE_MAP()

// 调度映射
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// 事件映射
	DECLARE_EVENT_MAP()

// 调度和事件 ID
public:
	enum {
		dispidGetSomeNumber = 1L
	};
protected:
	LONG GetSomeNumber();
};

