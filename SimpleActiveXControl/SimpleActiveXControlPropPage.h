#pragma once

// SimpleActiveXControlPropPage.h : CSimpleActiveXControlPropPage 属性页类的声明。


// CSimpleActiveXControlPropPage : 有关实现的信息，请参阅 SimpleActiveXControlPropPage.cpp。

class CSimpleActiveXControlPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CSimpleActiveXControlPropPage)
	DECLARE_OLECREATE_EX(CSimpleActiveXControlPropPage)

// 构造函数
public:
	CSimpleActiveXControlPropPage();

// 对话框数据
	enum { IDD = IDD_PROPPAGE_SIMPLEACTIVEXCONTROL };

// 实现
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 消息映射
protected:
	DECLARE_MESSAGE_MAP()
};

