

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Sun Feb 19 15:04:09 2017
 */
/* Compiler settings for SimpleActiveXControl.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __SimpleActiveXControlidl_h__
#define __SimpleActiveXControlidl_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef ___DSimpleActiveXControl_FWD_DEFINED__
#define ___DSimpleActiveXControl_FWD_DEFINED__
typedef interface _DSimpleActiveXControl _DSimpleActiveXControl;

#endif 	/* ___DSimpleActiveXControl_FWD_DEFINED__ */


#ifndef ___DSimpleActiveXControlEvents_FWD_DEFINED__
#define ___DSimpleActiveXControlEvents_FWD_DEFINED__
typedef interface _DSimpleActiveXControlEvents _DSimpleActiveXControlEvents;

#endif 	/* ___DSimpleActiveXControlEvents_FWD_DEFINED__ */


#ifndef __SimpleActiveXControl_FWD_DEFINED__
#define __SimpleActiveXControl_FWD_DEFINED__

#ifdef __cplusplus
typedef class SimpleActiveXControl SimpleActiveXControl;
#else
typedef struct SimpleActiveXControl SimpleActiveXControl;
#endif /* __cplusplus */

#endif 	/* __SimpleActiveXControl_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_SimpleActiveXControl_0000_0000 */
/* [local] */ 

#pragma once
#pragma region Desktop Family
#pragma endregion


extern RPC_IF_HANDLE __MIDL_itf_SimpleActiveXControl_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_SimpleActiveXControl_0000_0000_v0_0_s_ifspec;


#ifndef __SimpleActiveXControlLib_LIBRARY_DEFINED__
#define __SimpleActiveXControlLib_LIBRARY_DEFINED__

/* library SimpleActiveXControlLib */
/* [control][version][uuid] */ 


EXTERN_C const IID LIBID_SimpleActiveXControlLib;

#ifndef ___DSimpleActiveXControl_DISPINTERFACE_DEFINED__
#define ___DSimpleActiveXControl_DISPINTERFACE_DEFINED__

/* dispinterface _DSimpleActiveXControl */
/* [uuid] */ 


EXTERN_C const IID DIID__DSimpleActiveXControl;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("C7AE2ADF-F37B-4BC8-B9E3-09FFD5519247")
    _DSimpleActiveXControl : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DSimpleActiveXControlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DSimpleActiveXControl * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DSimpleActiveXControl * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DSimpleActiveXControl * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DSimpleActiveXControl * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DSimpleActiveXControl * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DSimpleActiveXControl * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DSimpleActiveXControl * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } _DSimpleActiveXControlVtbl;

    interface _DSimpleActiveXControl
    {
        CONST_VTBL struct _DSimpleActiveXControlVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DSimpleActiveXControl_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DSimpleActiveXControl_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DSimpleActiveXControl_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DSimpleActiveXControl_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DSimpleActiveXControl_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DSimpleActiveXControl_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DSimpleActiveXControl_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DSimpleActiveXControl_DISPINTERFACE_DEFINED__ */


#ifndef ___DSimpleActiveXControlEvents_DISPINTERFACE_DEFINED__
#define ___DSimpleActiveXControlEvents_DISPINTERFACE_DEFINED__

/* dispinterface _DSimpleActiveXControlEvents */
/* [uuid] */ 


EXTERN_C const IID DIID__DSimpleActiveXControlEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("C94158F6-FDF9-4884-93B6-03E1876FE2AD")
    _DSimpleActiveXControlEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _DSimpleActiveXControlEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _DSimpleActiveXControlEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _DSimpleActiveXControlEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _DSimpleActiveXControlEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _DSimpleActiveXControlEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _DSimpleActiveXControlEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _DSimpleActiveXControlEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _DSimpleActiveXControlEvents * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } _DSimpleActiveXControlEventsVtbl;

    interface _DSimpleActiveXControlEvents
    {
        CONST_VTBL struct _DSimpleActiveXControlEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _DSimpleActiveXControlEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _DSimpleActiveXControlEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _DSimpleActiveXControlEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _DSimpleActiveXControlEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _DSimpleActiveXControlEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _DSimpleActiveXControlEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _DSimpleActiveXControlEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___DSimpleActiveXControlEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_SimpleActiveXControl;

#ifdef __cplusplus

class DECLSPEC_UUID("47206538-8F44-4AAE-A622-B2BCB8247E19")
SimpleActiveXControl;
#endif
#endif /* __SimpleActiveXControlLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


