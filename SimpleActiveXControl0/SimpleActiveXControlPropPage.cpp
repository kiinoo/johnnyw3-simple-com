// SimpleActiveXControlPropPage.cpp : Implementation of the CSimpleActiveXControlPropPage property page class.

#include "stdafx.h"
#include "SimpleActiveXControl.h"
#include "SimpleActiveXControlPropPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CSimpleActiveXControlPropPage, COlePropertyPage)



// Message map

BEGIN_MESSAGE_MAP(CSimpleActiveXControlPropPage, COlePropertyPage)
END_MESSAGE_MAP()



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CSimpleActiveXControlPropPage, "SIMPLEACTIVEXC.SimpleActiveXCPropPage.1",
	0xaa8cd9b, 0x15e6, 0x4247, 0xa1, 0xca, 0xc3, 0x92, 0x60, 0x1, 0x4a, 0x3c)



// CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CSimpleActiveXControlPropPage

BOOL CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_SIMPLEACTIVEXCONTROL_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}



// CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPage - Constructor

CSimpleActiveXControlPropPage::CSimpleActiveXControlPropPage() :
	COlePropertyPage(IDD, IDS_SIMPLEACTIVEXCONTROL_PPG_CAPTION)
{
}



// CSimpleActiveXControlPropPage::DoDataExchange - Moves data between page and properties

void CSimpleActiveXControlPropPage::DoDataExchange(CDataExchange* pDX)
{
	DDP_PostProcessing(pDX);
}



// CSimpleActiveXControlPropPage message handlers
