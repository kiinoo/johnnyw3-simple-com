#pragma once

// SimpleActiveXControlCtrl.h : Declaration of the CSimpleActiveXControlCtrl ActiveX Control class.


// CSimpleActiveXControlCtrl : See SimpleActiveXControlCtrl.cpp for implementation.

class CSimpleActiveXControlCtrl : public COleControl
{
	DECLARE_DYNCREATE(CSimpleActiveXControlCtrl)

// Constructor
public:
	CSimpleActiveXControlCtrl();

// Overrides
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual DWORD GetControlFlags();

// Implementation
protected:
	~CSimpleActiveXControlCtrl();

	DECLARE_OLECREATE_EX(CSimpleActiveXControlCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CSimpleActiveXControlCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CSimpleActiveXControlCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CSimpleActiveXControlCtrl)		// Type name and misc status

// Message maps
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	DECLARE_DISPATCH_MAP()

// Event maps
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
		dispidGetSomeNumber2 = 2L,
		dispidGetSomeNumber = 1L
	};
protected:
	LONG GetSomeNumber(void);
	LONG GetSomeNumber2();
};

