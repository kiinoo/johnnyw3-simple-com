// SimpleActiveXControlCtrl.cpp : Implementation of the CSimpleActiveXControlCtrl ActiveX Control class.

#include "stdafx.h"
#include "SimpleActiveXControl.h"
#include "SimpleActiveXControlCtrl.h"
#include "SimpleActiveXControlPropPage.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CSimpleActiveXControlCtrl, COleControl)



// Message map

BEGIN_MESSAGE_MAP(CSimpleActiveXControlCtrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()



// Dispatch map

BEGIN_DISPATCH_MAP(CSimpleActiveXControlCtrl, COleControl)
	DISP_FUNCTION_ID(CSimpleActiveXControlCtrl, "GetSomeNumber", dispidGetSomeNumber, GetSomeNumber, VT_I4, VTS_NONE)
	DISP_FUNCTION_ID(CSimpleActiveXControlCtrl, "GetSomeNumber2", dispidGetSomeNumber2, GetSomeNumber2, VT_I4, VTS_NONE)
END_DISPATCH_MAP()



// Event map

BEGIN_EVENT_MAP(CSimpleActiveXControlCtrl, COleControl)
END_EVENT_MAP()



// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CSimpleActiveXControlCtrl, 1)
	PROPPAGEID(CSimpleActiveXControlPropPage::guid)
END_PROPPAGEIDS(CSimpleActiveXControlCtrl)



// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CSimpleActiveXControlCtrl, "SIMPLEACTIVEXCON.SimpleActiveXConCtrl.1",
	0x47206538, 0x8f44, 0x4aae, 0xa6, 0x22, 0xb2, 0xbc, 0xb8, 0x24, 0x7e, 0x19)



// Type library ID and version

IMPLEMENT_OLETYPELIB(CSimpleActiveXControlCtrl, _tlid, _wVerMajor, _wVerMinor)



// Interface IDs

const IID IID_DSimpleActiveXControl = { 0xC7AE2ADF, 0xF37B, 0x4BC8, { 0xB9, 0xE3, 0x9, 0xFF, 0xD5, 0x51, 0x92, 0x47 } };
const IID IID_DSimpleActiveXControlEvents = { 0xC94158F6, 0xFDF9, 0x4884, { 0x93, 0xB6, 0x3, 0xE1, 0x87, 0x6F, 0xE2, 0xAD } };


// Control type information

static const DWORD _dwSimpleActiveXControlOleMisc =
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CSimpleActiveXControlCtrl, IDS_SIMPLEACTIVEXCONTROL, _dwSimpleActiveXControlOleMisc)



// CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CSimpleActiveXControlCtrl

BOOL CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_SIMPLEACTIVEXCONTROL,
			IDB_SIMPLEACTIVEXCONTROL,
			afxRegApartmentThreading,
			_dwSimpleActiveXControlOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}



// CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrl - Constructor

CSimpleActiveXControlCtrl::CSimpleActiveXControlCtrl()
{
	InitializeIIDs(&IID_DSimpleActiveXControl, &IID_DSimpleActiveXControlEvents);
	// TODO: Initialize your control's instance data here.
}



// CSimpleActiveXControlCtrl::~CSimpleActiveXControlCtrl - Destructor

CSimpleActiveXControlCtrl::~CSimpleActiveXControlCtrl()
{
	// TODO: Cleanup your control's instance data here.
}



// CSimpleActiveXControlCtrl::OnDraw - Drawing function

void CSimpleActiveXControlCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	if (!pdc)
		return;

	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->Ellipse(rcBounds);
}



// CSimpleActiveXControlCtrl::DoPropExchange - Persistence support

void CSimpleActiveXControlCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
}



// CSimpleActiveXControlCtrl::GetControlFlags -
// Flags to customize MFC's implementation of ActiveX controls.
//
DWORD CSimpleActiveXControlCtrl::GetControlFlags()
{
	DWORD dwFlags = COleControl::GetControlFlags();


	// The control can activate without creating a window.
	// TODO: when writing the control's message handlers, avoid using
	//		the m_hWnd member variable without first checking that its
	//		value is non-NULL.
	dwFlags |= windowlessActivate;
	return dwFlags;
}



// CSimpleActiveXControlCtrl::OnResetState - Reset control to default state

void CSimpleActiveXControlCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}



// CSimpleActiveXControlCtrl message handlers


LONG CSimpleActiveXControlCtrl::GetSomeNumber(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return 123;
}


LONG CSimpleActiveXControlCtrl::GetSomeNumber2()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO:  在此添加调度处理程序代码

	return 0;
}
