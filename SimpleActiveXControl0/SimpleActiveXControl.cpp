// SimpleActiveXControl.cpp : Implementation of CSimpleActiveXControlApp and DLL registration.

#include "stdafx.h"
#include "SimpleActiveXControl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CSimpleActiveXControlApp theApp;

const GUID CDECL _tlid = { 0xFC0D3710, 0xF032, 0x405C, { 0x83, 0x60, 0x45, 0xFC, 0xB8, 0x7, 0x73, 0xC2 } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;



// CSimpleActiveXControlApp::InitInstance - DLL initialization

BOOL CSimpleActiveXControlApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: Add your own module initialization code here.
	}

	return bInit;
}



// CSimpleActiveXControlApp::ExitInstance - DLL termination

int CSimpleActiveXControlApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}



// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}



// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
