#pragma once

// SimpleActiveXControlPropPage.h : Declaration of the CSimpleActiveXControlPropPage property page class.


// CSimpleActiveXControlPropPage : See SimpleActiveXControlPropPage.cpp for implementation.

class CSimpleActiveXControlPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CSimpleActiveXControlPropPage)
	DECLARE_OLECREATE_EX(CSimpleActiveXControlPropPage)

// Constructor
public:
	CSimpleActiveXControlPropPage();

// Dialog Data
	enum { IDD = IDD_PROPPAGE_SIMPLEACTIVEXCONTROL };

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	DECLARE_MESSAGE_MAP()
};

