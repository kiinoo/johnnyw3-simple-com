﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SimpleWinFormsApp
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            
            var c = new SimpleActiveXControlLib.SimpleActiveXControl();
            var form = new Form
            {
                Controls = { c }
            };
            form.Load += (s, e) => c.GetSomeNumber();
            Application.Run(form); 
        }
    }
}
